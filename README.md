# Local-first software

This repository contains the LaTeX and HTML sources of the article “Local-first
software: You own your data, in spite of the cloud” by Martin Kleppmann, Adam
Wiggins, Peter van Hardenberg, and Mark McGranaghan at Onward! Essays 2019.

The article is available at https://www.inkandswitch.com/local-first.html
and as a PDF file in this repository.

## Build

Install LaTeX (such as https://www.tug.org/mactex/ -- `brew cask install mactex-no-gui`)

Then:

    pdflatex local-first && bibtex local-first && pdflatex local-first
    open local-first.pdf

